function clone(obj){
    let cloneObj = {};
    
    if(typeof obj !== 'object' || obj == null){
        
        return obj;
    }
    for(let key in obj){
        if(obj.hasOwnProperty(key)){
            cloneObj[key] = clone(obj[key]);
        }
    }
    return cloneObj;    
}
const obj = {
	name : "Misha",
	age: 16,
	smth:{
		city:"Kiyv"
		}
};

const cloneObj = clone(obj);

console.log(obj);
console.log(cloneObj);