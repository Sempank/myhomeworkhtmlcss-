function filterCollection(arr, keywords, findAllK, ...directions) {
    keywords = keywords.split(' ');
    return arr.filter(obj => {
        if (findAllK) {
            for (let keyword of keywords) {
                if (!findValue(obj, keyword, directions))
                    return false;
                return true;
            }
        }
        for (let keyword of keywords)
            if (findValue(obj, keyword, directions))
                return true;
        return false;
    })
}
let movies = [{
        director: 'Zemekis',
        country: 'US',
        language: 'ENG'
    },
    {
        director: 'Zemekis',
        country: 'US',
        language: 'RU'
    },
    {
        director: 'Zemekis',
        country: 'RU',
        language: 'RU'
    },
    {
        director: 'Spielberg',
        country: 'US',
        language: 'ENG'
    },
    {
        director: 'Spielberg',
        country: 'US',
        language: 'ENG'
    },
    {
        director: {
            name: {
                ser: {
                    a: 'Spielberg'
                }
            }
        },
        country: 'RU',
        language: 'RU'
    },
    {
        director: 'Tarkovsky',
        country: 'RU',
        language: 'RU'
    },
    {
        director: 'Tarkovsky',
        country: 'RU',
        language: 'RU'
    },
    {
        director: {
            name: 'Tarkovsky'
        },
        country: {
            eu: 'RU'
        },
        language: 'RU'
    }
];
console.log(filterCollection(movies, 'Spielberg', true, 'director.name.ser.a', ));
console.log(filterCollection(movies, 'Tarkovsky RU', false, 'director.name', 'country.eu'));
var country = [{
    name: "Швейцария",
    languages: ["немецкий", "французский", "итальянский"]
},
{
    capital: {
        name: "Берн",
        population: 126598
    }
},
{
    citie: {
            name: "Цюрих",
            population: 378884
        }      
    
}
];
console.log(filterCollection(country, 'Берн 126598', true, 'capital.name', 'capital.population'));
console.log(filterCollection(country, 'Цюрих', false, 'citie.name'));

/*FUNCTIONS*/

function findValue(obj, val, directions) {
    let dirArr = directions.map(direction => direction.split('.'));
    let reservObj = obj;
    for (let direction of dirArr) {
        for (let step of direction) {
            if (typeof obj[step] === 'object') {
                obj = obj[step];
                continue;
            }
            if (obj[step] === val) {
                return true;
            }
        }
        obj = reservObj;
    }
    return false;
}