if (!localStorage.getItem('theme')) {
    localStorage.setItem('theme', 'styles/blue.css');
}

const cssStyle = document.getElementById('page-theme');
let hrefAttr = cssStyle.getAttribute('href');
let changeBtn = document.getElementById("changeBtn");
let changeBtn1 = document.getElementById("changeBtn1");
hrefAttr = cssStyle.setAttribute('href', localStorage.getItem('theme'));

changeBtn.addEventListener('click', changeColor);
changeBtn1.addEventListener('click', changeColor);

function changeColor() {

    if (localStorage.getItem('theme') === 'styles/red.css') {
        localStorage.setItem('theme', 'styles/blue.css');
        cssStyle.setAttribute('href', 'styles/blue.css');
    } else {
        localStorage.setItem('theme', 'styles/red.css');

        cssStyle.setAttribute('href', localStorage.getItem('theme'));
        console.log(localStorage.getItem('theme'));
    }
}