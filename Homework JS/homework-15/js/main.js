let m = 0,
   s = 0,
   ms = 0;

let start = () => {
   startBtn.style.display = "none";
   pauseBtn.style.display = "block";
   timer();
}

let pause = () => {
   clearInterval(startTimer);
   pauseBtn.style.display = "none";
   startBtn.style.display = "block";

}
let clear = () => {
   pause();
   m = s = ms = 0;
   updateTimer();

}

let updateTimer = () => {
   minutes.innerText = `${addZero(m)}`;
   seconds.innerText = `${addZero(s)}`;
   miliseconds.innerText = `${addZero(ms)}`;
}

let timer = () => {
   startTimer = setInterval(() => {
      ms++;
      if (ms === 100) {
         s++;
         ms = 0;
      }
      if (s === 60) {
         m++;
         s = 0;
      }
      if (m === 60)
         m = 0;
      updateTimer();
   }, 10);
}

let addZero = number => (number < 10) ? `0${number}` : `${number}`;

let startBtn = document.querySelector('.start-btn');
let pauseBtn = document.querySelector('.pause-btn');
let clearBtn = document.querySelector('.clear-btn');

startBtn.addEventListener('click', start);
pauseBtn.addEventListener('click', pause);
clearBtn.addEventListener('click', clear);

let startTimer = null;
let minutes = document.querySelector('.minutes');
let seconds = document.querySelector('.seconds');
let miliseconds = document.querySelector('.miliseconds');