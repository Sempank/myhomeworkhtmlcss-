let me = new CreateNewUser();

function CreateNewUser() {
    this.firstName = getString('Enter your name');
    this.lastName = getString('Enter your last name');
    this.getLogin = () => (this.firstName.charAt(0) + this.lastName).toLowerCase();

    Object.defineProperty(this, 'firstName', {
        writable: false,
        configurable: true
    });

    this.setFirstName = function (value) {
        Object.defineProperty(this, 'firstName', {
            value: value
        });
    }

    Object.defineProperty(this, 'lastName', {
        writable: false,
        configurable: true
    });
}

function getString(message, errorMessage) {
    let string = prompt(message);
    while (!string) {
        string = prompt(errorMessage);
    };
    return string;
}

 